<aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="img/avatar3.png" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Hello, <?php echo $session_usertype; ?></p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="active">
                            <a href="admin_home.php">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                      
                      <!-- Master's Management -->
                      
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-bar-chart-o"></i>
                                <span>Master's Management</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                            
                                
   
  							<li><a href="industype_master.php"><i class="fa fa-angle-double-right"></i>Industry Type Master</a></li>
     
  							
<!--  							<li><a href="master_jobtype.php"><i class="fa fa-angle-double-right"></i>Job Type Master</a></li>-->
  
 							<li><a href="master_banner.php"><i class="fa fa-angle-double-right"></i>Banner Master</a></li>
  
  							<li><a href="scheduled_banner.php"><i class="fa fa-angle-double-right"></i>Scheduled Banner</a></li>
                      
                            </ul>
                        </li>
                        
                        <li class="treeview">
                        <a href="#">
                                <i class="fa fa-location-arrow"></i>
                                <span>Location Management</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                          <ul class="treeview-menu">
                          <li><a href="master_country.php"><i class="fa fa-angle-double-right"></i>Countries Master</a></li>
                          <li><a href="master_city.php"><i class="fa fa-angle-double-right"></i>City Master</a></li>
                          <li><a href="master_state.php"><i class="fa fa-angle-double-right"></i>State Master</a></li>
                          
                          </ul> 
                        </li>
                        
                         <!-- Pricing Management -->
                         
                        <li>
                            <a href="mem_priceform.php">
                                <i class="fa fa-usd"></i>
                                <span>Pricing Management</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            
                        </li>
                        
                        <!-- Employer Management -->
                        
                        <li >
                            <a href="emp_manage.php">
                                <i class="fa fa-user"></i> <span>Employer Management</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            
                        </li>
                        <!-- Employer Management -->
                        
                        <li>
                            <a href="jobs_manage.php">
                                <i class="fa fa-tasks"></i> <span>Jobs Management</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            
                        </li>
                        
                        <!-- JobSeeker's Management -->
                        
                        <li >
                            <a href="seek_manage.php">
                                <i class="fa fa-user-md"></i> <span>JobSeeker's Management</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            
                        </li>
                        
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-tasks"></i> <span>Email Content Management</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href=""><i class="fa fa-angle-double-right"></i>Forget Password(Seeker)</a></li>
                                <li><a href=""><i class="fa fa-angle-double-right"></i>Forget Password(Employer)</a></li>
                                <li><a href="reg_seekcontent.php"><i class="fa fa-angle-double-right"></i>Registration(Seeker's)</a></li>
                                <li><a href="reg_empcontent.php"><i class="fa fa-angle-double-right"></i>Registration(Employer's)</a></li>
                            </ul>
                        </li>
                        
                        
                         <!-- Front Page  Management -->
                        
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-clipboard"></i> <span>Front Page Management</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href=""><i class="fa fa-angle-double-right"></i> Resume Tips</a></li>
                                <li><a href=""><i class="fa fa-angle-double-right"></i> Interview Tips</a></li>
                                <li><a href="terms_condition.php"><i class="fa fa-angle-double-right"></i> Terms & Conditions</a></li>
                                <li><a href="about_us.php"><i class="fa fa-angle-double-right"></i> About Us</a></li>
                            </ul>
                        </li>
                        
                          <!-- Administration Security Page -->
                        
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-lock"></i> <span>Administration Security</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="quick_mail.php"><i class="fa fa-angle-double-right"></i>Quick Mail</a></li>                           
                            </ul>
                        </li>
                        
                        <!-- Calendar -->
                        
                        <!--<li>
                            <a href="pages/calendar.html">
                                <i class="fa fa-calendar"></i> <span>Calendar</span>
                                <small class="badge pull-right bg-red">3</small>
                            </a>
                        </li>-->
                        
                        <!-- Mail Box-->
                        
                       <!-- <li>
                            <a href="pages/mailbox.html">
                                <i class="fa fa-envelope"></i> <span>Mailbox</span>
                                <small class="badge pull-right bg-yellow">12</small>
                            </a>
                        </li>-->
                        
                        
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
            
            
            