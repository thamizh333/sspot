<?php 
error_reporting(0);
include "admin_session.php";
if(isset($_POST['submit'])) {
	
	$pname = $_POST['pname'];
	$pprice = $_POST['pprice'];
	$pdesc = $_POST['pdesc'];
	$terms_cond = $_POST['terms_cond'];
	$vperiod = $_POST['vperiod'];
	$order_disp = $_POST['order_disp'];
	$resume_down = $_POST['resume_down'];
	$no_posting = $_POST['no_posting'];
	$no_users = $_POST['no_users'];
	
	$pname = (filter_var($_POST['pname'], FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => "/^[a-zA-Z. ]+$/")))); 
	
	$e_query = mysql_query("select id from package_price where pname='$pname'");
	$e_sq=mysql_num_rows($e_query);
	
	if ($pname == "") {
	$err_pname = "Valid Package Name is required.        ";
	}else if(!$e_sq=='') {
	$err_pname = "$pname is already in use.";
	}
	

	if ($email == ""){
		 $errors .= '.';
	}else if(!$e_sq=='') {
	$err_email = "$email is already in use.";

	}
	
	if(!empty($pprice)){
		
		if(preg_match('/^-?(?:\d+|\d*\.\d+)$/', $pprice)) {
				
				$pprice = $pprice;
				
		} else {	
			$err_ppprice = 'Only Numeric values allowed';
		}
	} else {
			$err_ppprice = 'Provide Package Price';		
	}
	
		if($pdesc == "")
		$err_pdesc = "Package description Must";

		if($terms_cond == "")
		$err_terms_cond = "Terms & Condition Must";
		
		if(!is_numeric($vperiod))
		$err_vperiod = 'Only Numeric values';
		
		if(!is_numeric($order_disp))
		$err_order_disp = 'Only Numeric values';

		if(!is_numeric($resume_down))
		$err_resume_down = 'Only Numeric values';
		
		if(!is_numeric($no_posting))
		$err_no_posting = 'Only Numeric values';

		if(!is_numeric($no_users))
		$err_no_users = 'Only Numeric values';
	
	
	$error .= $err_pname.$err_ppprice.$err_pdesc.$err_terms_cond.$err_vperiod.$err_order_disp.$err_resume_down.$err_no_posting.$err_no_users;
	
	$validation_check = "";
	
	if(isset($error)) 
	$validation_check.=$error; 
	
	
	if (!$validation_check) {
	
	
	
  $query =	mysql_query("INSERT INTO package_price(pname,pprice,p_des,terms_cond,val_period,order_dis,resume_download,no_posting,no_users)VALUES('".$pname."','".$pprice."','".$pdesc."','".$terms_cond."','".$vperiod."','".$order_disp."','".$resume_down."','".$no_posting."','".$no_users."')");
?>
<script>
alert("<?php echo $pname." "."Package created Successfully" ?>");
window.location = "admin_home.php";
</script>
<?php  
  		
	}
}


 ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Price Management | StaffingSpot</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
        
      

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <?php include "includes/header.php"; ?>
        
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <?php include "includes/side_menu.php"; ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Package Pricing Management
                        <small>it all starts here</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="admin_home.php"><i class="fa fa-dashboard"></i> Home</a></li>                        <li class="active">Package Pricing</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
					
                    <div class="panel panel-default">
<div class="panel-heading"><h3 class="panel-title">Package Pricing Management</h3></div>
<div class="panel-body">
<form class="form-horizontal" action="" method="post" enctype="multipart/form-data">

 <div class="form-group clr_both">
 <label class="col-sm-2 control-label" style="margin-left:3%;">Package Name *</label>
<div class="col-sm-3"><input type="text" class="form-control" name="pname" placeholder="Package Name *" value="<?php echo $_POST['pname']; ?>"/><span class="err_txt help-block"><?php echo $err_pname ; ?>
</span></div>

 <label class="col-sm-2 control-label" style="margin-left:3%;">Package Price *</label>
<div class="col-sm-3"><input type="text" class="form-control" name="pprice"  placeholder="Package Price *" value="<?php echo $_POST['pprice']; ?>"/><span class="err_txt"><?php echo $err_ppprice ; ?></span></div>
</div>

 <div class="form-group clr_both">
 <label class="col-sm-2 control-label" style="margin-left:3%;">Package Description *</label>
<div class="col-sm-3"><textarea name="pdesc" class="form-control" rows="2" cols="15" placeholder="Package Description *"><?php echo $_POST['pdesc']; ?></textarea><span class="err_txt"><?php echo $err_pdesc; ?></span></div>

 <label class="col-sm-2 control-label" style="margin-left:3%;">Terms & Conditions *</label>
<div class="col-sm-3"><textarea name="terms_cond" class="form-control" rows="2" cols="15" placeholder="Terms & Conditions *"><?php echo $_POST['terms_cond']; ?></textarea><span class="err_txt"><?php echo $err_terms_cond; ?></span></div>
</div>

 <div class="form-group clr_both">
 <label class="col-sm-2 control-label" style="margin-left:3%;">Validity Period *</label>
<div class="col-sm-3"><input type="text" name="vperiod" class="form-control" placeholder="Validity Period *" value="<?php echo $_POST['vperiod']; ?>"/><span class="err_txt"><?php echo $err_vperiod; ?></span></div>

 <label class="col-sm-2 control-label" style="margin-left:3%;">Order of Display *</label>
<div class="col-sm-3"><input type="text" class="form-control" name="order_disp" placeholder="Order of Display  *" value="<?php echo $_POST['order_disp']; ?>"/><span class="err_txt"><?php echo $err_order_disp; ?></span></div>
</div>

<div class="form-group clr_both">
 <label class="col-sm-2 control-label" style="margin-left:-2%;" ><h4 class="text-danger" >Restriction On :</h4></label>	
</div>
 <div class="form-group clr_both">
 <label class="col-sm-2 control-label" style="margin-left:3%;">Resume Downloaded *</label>
<div class="col-sm-3"><input type="text" class="form-control" name="resume_down" placeholder="Resume Downloaded  *" value="<?php echo $_POST['resume_down']; ?>" /><span class="err_txt"><?php echo $err_resume_down; ?></span></div>
</div>

 <div class="form-group clr_both">
  <label class="col-sm-2 control-label" style="margin-left:3%;">No of Posting *</label>
<div class="col-sm-3"><input type="text" class="form-control" name="no_posting" placeholder="No of Posting  *" value="<?php echo $_POST['no_posting']; ?>" /><span class="err_txt"><?php echo $err_no_posting; ?></span>
</div>
</div>

 <div class="form-group clr_both">
 <label class="col-sm-2 control-label" style="margin-left:3%;">No of Users *</label>

<div class="col-sm-3">
<input type="text" class="form-control" name="no_users" placeholder="No of Users *" value="<?php echo $_POST['no_users']; ?>" /><span class="err_txt"><?php echo $err_no_users; ?></span>
<span class="err_txt"></span></div>
</div>

<label class="col-sm-2 control-label clr_both" style="margin-left:3%;"></label>
<div class="col-sm-4">
<input type="submit" class="btn btn-warning" name="submit"  value="Create Package"/>
<input type="reset" class="btn btn-warning" name="reset"  value="Reset"/>
<a class="btn btn-warning" style="text-decoration:none;" href="admin_home.php">Back</a>
</div>
</form>


<br/>
<br/>

<div class="col-md-10" style="margin-top:45px;">
<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover">
            <tr class="danger"><th class="text-center">Package Name</th><th class="text-center">Package Price</th><th class="text-center">Validity Period</th><th class="text-center">Resume Downloaded</th><th class="text-center">No of Posting</th><th class="text-center" colspan="2">Options</th></tr>
      <?php $price_query = mysql_query("select * from package_price");
			while($price_array = mysql_fetch_array($price_query)) {
			$id = $price_array['id'];	
			$name = $price_array['pname'];
			$price = $price_array['pprice'];
			$valdity = $price_array['val_period'];
			$resume = $price_array['resume_download'];
			$posting = $price_array['no_posting'];
			?>
            <tr align="center"><td><?php echo $name; ?></td><td><?php echo $price; ?></td><td><?php echo $valdity; ?></td><td><?php echo $resume; ?></td><td><?php echo $posting; ?></td><td><a href="edit_price.php?id=<?php echo $id; ?>">Edit</a></td><td><a href="#" onclick="if (confirm('Are you sure to delete this package?')) 
  window.location='delete_package.php?id=<?php echo $id; ?>';">Delete</a></td></tr>
            <?php } ?>
           </table>
          
          </div>






</div>
</div>
</div>
                   
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="js/AdminLTE/demo.js" type="text/javascript"></script>
        
    </body>
</html>
