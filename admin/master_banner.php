<?php error_reporting(0);
include "admin_session.php"; 

if(isset($_POST['submit'])) {

     $title = $_POST['title'];
	 $descrip = $_POST['descrip'];
	 
	 if($title == "")
	 $err_title = "Banner title is mandatory";
	 
	 if($descrip == "")
	 $err_descrip = "Banner Description is mandatory";


$allowedExts = array("jpg", "jpeg", "gif", "png");
$temp = explode(".", $_FILES["banner"]["name"]);
$extension = end($temp);
$RandomNum   = rand(0, 9999999999);
if ((($_FILES["banner"]["type"] == "image/gif")
 || ($_FILES["banner"]["type"] == "image/jpeg")
 || ($_FILES["banner"]["type"] == "image/png")
 || ($_FILES["banner"]["type"] == "image/jpg"))
&& ($_FILES["banner"]["size"] < 20000000000)
&& in_array($extension, $allowedExts)) {
  if ($_FILES["banner"]["error"] > 0) {
    "Return Code: " . $_FILES["banner"]["error"] . "<br>";
  } else {
	 move_uploaded_file($_FILES["banner"]["tmp_name"],
      "banner/" .$RandomNum. $_FILES["banner"]["name"]);
      $logo =  "banner/" .$RandomNum. $_FILES["banner"]["name"];
  }
} else {
  $err_upload =  "Invalid file";  
}


    $error .= $err_title.$err_descrip.$err_upload;
	
	$validation_check = "";
	if(isset($error))
	$validation_check .= $error; 
	
	if(!$validation_check){
	
	$query = mysql_query("insert into admin_slider(title,descrip,image,active)values('".$title."','".$descrip."','".$logo."','2')");
	}
	if($query) {
	?>
	<script>
	alert("Banner Added Successfully");
	window.location ="master_banner.php";
	</script>
<?php
	}
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Banner | StaffingSpot</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
        
      

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <?php include "includes/header.php"; ?>
        
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <?php include "includes/side_menu.php"; ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Banner Master
                        <small>it all starts here</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="admin_home.php"><i class="fa fa-dashboard"></i> Home</a></li>                        <li class="active">Banner Master</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
					
                    <div class="panel panel-default">
<div class="panel-heading"><h3 class="panel-title">Banner Master</h3></div>
<div class="panel-body">
<form class="form_top_space" action="" method="post" enctype="multipart/form-data" role="form">
<div class="form-group">
    <label >Image Title *</label>
    <input type="text" class="form-control" name="title" placeholder="Image Title">
    <span class="help-block"><?php echo $err_title; ?></span>
  </div>
<div class="form-group">
    <label >Image Description *</label>
    <textarea name="descrip" class="form-control"></textarea>
    <span class="help-block"><?php echo $err_descrip; ?></span>
  </div>
<div class="form-group">
    <label >Upload Banner</label>
    <input type="file" data-filename-placement="inside" name="banner">&nbsp;&nbsp;&nbsp;&nbsp;
    <font color="#FF0000">Only JPG,PNG,JPEG,GIF format allowed</font>
    <span class="help-block"><?php echo $err_upload; ?></span>
  </div>
<input type="submit" name="submit" class="btn btn-warning"  value="Upload Banner"/>
<input type="button" onClick="location.href='admin_home.php'" class="btn btn-warning" value="Back" />
</form>


<br/>
<br/>

<div class="col-md-10" style="margin-top:45px;">
<div class="table-responsive" style="margin-top:45px;"  >
<table class="table table-bordered table-hover ">
<th class="text-center info">SI NO</th>
<th class="text-center info">TITLE</th>
<th class="text-center info">DESCRIPTION</th>
<th class="text-center info">IMAGE</th>
<th class="text-center info">STATUS</th>
<th class="text-center info">OPTION</th>
<?php $image_query = mysql_query("select * from admin_slider where fromdate ='0000-00-00'"); 
$a=0;
while($image_fetch = mysql_fetch_array($image_query)) {
	$view_id = $image_fetch['id'];
$view_title = $image_fetch['title'];
$view_descrip = $image_fetch['descrip'];
$view_image = "http://www.staffingspot.in/admin/".$image_fetch['image'];
$view_active = $image_fetch['active'];
$a++;

?>
<tr class="text-center">
<td><?php echo $a; ?></td>
<td><?php echo $view_title; ?></td>
<td><?php echo $view_descrip; ?></td>
<td><img src="<?php echo $view_image; ?>" width="50px" height="50px" /></td>
<td>
<?php if($view_active == "0") {
?>	
<a href="active_banner.php?imgid=<?php echo $view_id; ?>&active=<?php echo $view_active; ?>">InActive</a>
<?php	
} else { 
?>
<a href="active_banner.php?imgid=<?php echo $view_id; ?>&active=<?php echo $view_active; ?>">Active</a>
<?php
}?>
</td>
<td><a href="#"><i class="fa fa-trash-o"></i></a></td></tr>
<?php
}?>

</table>
</div>

</div>
</div>
</div>
                    
                   

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="js/AdminLTE/demo.js" type="text/javascript"></script>
        
<script type="text/javascript" src="js/bootstrap.file-input.js"></script>
    <script>	
	$(document).ready(function() {
		
	$('input[type=file]').bootstrapFileInput();
	$('.file-inputs').bootstrapFileInput();
	
	});
	</script>
    </body>
</html>
