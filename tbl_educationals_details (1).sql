-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 24, 2017 at 11:27 AM
-- Server version: 5.6.11
-- PHP Version: 5.5.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `staffing_jobportal`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_educationals_details`
--

CREATE TABLE IF NOT EXISTS `tbl_educationals_details` (
  `fld_id` int(15) NOT NULL AUTO_INCREMENT,
  `fld_educational` varchar(15) NOT NULL,
  `fld_course` varchar(15) NOT NULL,
  `fld_college` varchar(15) NOT NULL,
  `fld_from` year(4) NOT NULL,
  `fld_to` year(4) NOT NULL,
  `fld_cgpa` varchar(15) NOT NULL,
  `fld_status` int(15) NOT NULL DEFAULT '0',
  `fld_user_status` int(15) NOT NULL,
  PRIMARY KEY (`fld_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `tbl_educationals_details`
--

INSERT INTO `tbl_educationals_details` (`fld_id`, `fld_educational`, `fld_course`, `fld_college`, `fld_from`, `fld_to`, `fld_cgpa`, `fld_status`, `fld_user_status`) VALUES
(1, 'Arts', 'ECE', 'er4r test', 2014, 2016, '56', 2, 0),
(2, 'Arts', 'MECH/option>\n  ', 'er4rreretreertd', 2014, 2016, '5623', 2, 0),
(4, 'Arts', 'Cse', 'KSR67767', 2009, 1999, '7867676', 2, 0),
(17, 'Arts', 'Mech', 'bsc', 2013, 2013, '100', 2, 0),
(18, 'Master', 'IT', 'sns', 2017, 2012, '65.7', 2, 0),
(19, 'PG', '0', 'sns', 2012, 2010, '89', 2, 0),
(20, 'UG', 'ECE', 'PSG', 2013, 2009, '123', 2, 0),
(21, '0', 'Cse', 'PSG', 2016, 2014, '79', 2, 0),
(22, '0', 'Cse', 'PSG', 2016, 2014, '79', 2, 0),
(23, 'PG', 'ECE', 'PSG', 2015, 2012, '78', 2, 0),
(24, 'PG', 'ECE', 'PSG', 2015, 2012, '78', 2, 0),
(25, 'PG', 'ECE', 'PSG', 2015, 2012, '78', 2, 0),
(26, 'Arts', 'Cse', 'PSG', 2017, 2013, '65.7', 2, 0),
(27, 'PG', 'IT', 'PSG', 2014, 2005, '67', 2, 0),
(28, 'Master', 'IT', 'qe', 2013, 2011, '45', 2, 0),
(29, 'Arts', 'IT', 'sns', 2016, 2014, '65.7', 2, 0),
(30, 'UG', 'IT', 'PSG', 2008, 2003, '78', 2, 0),
(31, 'Master', 'ECE', 'PSG', 2012, 2016, '78', 2, 0),
(32, 'Arts', 'ECE', 'er4r', 2014, 2016, '56', 0, 0),
(33, 'Arts', 'ECE', 'er4rreretreert', 2014, 2016, '56', 0, 0),
(34, 'Arts', 'ECE', 'er4r', 2014, 2016, '56', 2, 0),
(35, 'Arts', 'ECE', 'er4r', 2014, 2016, '56', 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
