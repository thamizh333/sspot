<?php
@include("config.php");	
?><!DOCTYPE html>
<html lang="en">
<head>
    
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="vinformax">
    <title>Opportunities A Mega Job Board Template</title>
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">

    <!-- BOOTSTRAPE STYLESHEET CSS FILES -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!--JQUERY SELECT-->
    <link rel="stylesheet" href="css/select2.min.css">
	
    <!-- JQUERY MENU -->
    <link rel="stylesheet" href="css/mega_menu.min.css">

    <!-- ANIMATION -->
    <link rel="stylesheet" href="css/animate.min.css">

    <!-- OWl  CAROUSEL-->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.style.css">

    <!-- TEMPLATE CORE CSS -->
    <link rel="stylesheet" href="css/style.css">

    <!-- FONT AWESOME -->
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/et-line-fonts.css" type="text/css">

    <!-- Google Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700,900,300" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">

</head>

<body>
    <div class="page category-page">
        <div id="spinner">
            <div class="spinner-img">
                <img alt="Opportunities Preloader" src="images/loader.gif" />
                <h2>Please Wait.....</h2>
            </div>
        </div>

          <?php
        @include("top.php");
        ?>
    <div class="clearfix"></div>

        <div class="clearfix"></div>
      
        <section class="breadcrumb-search parallex">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 nopadding">
                        <div class="col-md-8 col-sm-12 col-md-offset-2 col-xs-12 nopadding">
                            <div class="search-form-contaner">
                                <form class="form-inline">
                                    <div class="col-md-7 col-sm-7 col-xs-12 nopadding" style="margin-left: 150px;">
                                        <div class="form-group">


                                            <input type="text" id="search" class="form-control" name="keyword" placeholder="Search Keyword" value="">

                                            <i class="icon-magnifying-glass"></i>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-xs-12 nopadding">
                                        <div class="form-group form-action">
                                            <button type="button" id="button" class="btn btn-default btn-search-submit">Search <i class="fa fa-angle-right"></i> </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="categories">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="Heading-title black">
                                <h1>Top Companies</h1>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium</p>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 nopadding loadmore">
                            <div id="cats-masonry">
                                
                                <?php 
                                $companylistsql= "SELECT e.fld_id,empd.fld_employer_name,empd.fld_logo,empd.fld_address FROM `tbl_employer` e Join tbl_employer_details empd on empd.fld_empid = e.fld_id WHERE e.`fld_emp_status`='1' ORDER BY e.`fld_id` LIMIT 0,9";
                                //echo $companylistsql;
                                $companylists=mysql_query($companylistsql);

                                while($row=mysql_fetch_assoc($companylists)) 
                                {  
                                     $companyid=$row['fld_id'];



                                ?>
          
                                <div class="col-md-4 col-sm-6 col-xs-12 editStyle">
                                    <a href="company_details.php?action=company_details&id=<?php echo $companyid; ?>">
                                            <img src="images/<?php echo $row['fld_logo'];?>" class="img-responsive" alt="">
                                        </div>  
                                        <div class="company-list-box-detail" style="width:75%; float:left;">
                                            <h5 style="color:#ED1D24; min-height: 60px;"> <?php  echo $row['fld_employer_name'];?> </h5>
                                            <p style="min-height:70px;"><?php echo mb_strimwidth(strip_tags($row['fld_address']), 0, 66, "..."); ?></p>
                                        </div>    
                                        <div class="ratings" style="width:100%; float:left; color:#ED1D24;">
                                            <i class="fa fa-star color"></i> 
                                            <i class="fa fa-star color"></i> 
                                            <i class="fa fa-star color"></i> 
                                            <i class="fa fa-star-half-full color"></i> 
                                            <i class="fa fa-star-o"></i>
                                            <span class="badge" style="float:right; background-color:#ED1D24;"> 4.5</span> 
                                        </div>
                                    </div>
                                    </a>
                                </div>
                                <?php }?>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="load-more-btn" id="load_more_main<?php echo $companyid; ?>">
                                        <button id="<?php echo $companyid;?>" class="btn-default load_more"> Load More <i class="fa fa-refresh"></i> </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

       

   
       <?php @include("bottom.php");?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <!-- JAVASCRIPT JS  -->
        <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>    
        <!-- JAVASCRIPT JS  --> 
        <script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>

        <!-- BOOTSTRAP CORE JS -->
        <script type="text/javascript" src="js/bootstrap.min.js"></script>

        <!-- JQUERY SELECT -->
        <script type="text/javascript" src="js/select2.min.js"></script>

        <!-- MEGA MENU -->
        <script type="text/javascript" src="js/mega_menu.min.js"></script>

         

        <!-- JQUERY COUNTERUP -->
        <script type="text/javascript" src="js/counterup.js"></script>

        <!-- JQUERY WAYPOINT -->
        <script type="text/javascript" src="js/waypoints.min.js"></script>

        <!-- JQUERY REVEAL -->
        <script type="text/javascript" src="js/footer-reveal.min.js"></script>

        <!-- Owl Carousel -->
        <script type="text/javascript" src="js/owl-carousel.js"></script>

        <!-- FOR THIS PAGE ONLY -->
        <script src="js/imagesloaded.js"></script>
        <script src="js/isotope.min.js"></script>
        
         <!-- CORE JS -->
        <script type="text/javascript" src="js/custom.js"></script>
        <script src="js/modernizr.js"></script>

        
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        
        <script type="text/javascript">
            (function($) {
                "use strict";
                $('#cats-masonry').imagesLoaded(function() {
                    $('#cats-masonry').isotope({
                        layoutMode: 'masonry',
                        transitionDuration: '0.3s'
                    });
                });
            })(jQuery);
        </script>

        <script>
            $(document).ready(function(){
//                alert("gfhfghg");
                $(document).on('click','.load_more',function(){             
//                    alert("gfhfghg");
                    var ID = $(this).attr('id');
                    $('.load_more').hide();
//                    $('.loding').show();
                    $.ajax({
                        type:'POST',
                        url:'ajax_company.php',
                        data:'action=load_more&id='+ID,
                        success:function(data){
                         //   alert("fghghfg");
                            $('#load_more_main'+ID).remove();
                            $('.loadmore').append(data)
                        }
                    }); 
                });
                $("#search").val("");
            });
            
        </script>
         <script>
                $(document).ready(function() {
                    //alert("test");
                  var mainbrand = [
                  <?php
                    $sdata="SELECT DISTINCT employerName FROM `employer` WHERE `status`='1'";
                    $results = mysql_query($sdata);
                    while($sqry = mysql_fetch_array($results, MYSQL_ASSOC))
                    {
                      $name = $sqry['employerName'];
                     $colon = "\"";
                     $comma = ",";
                     echo $colon.$name.$colon.$comma;
                    }
                   ?>
                  ];
                  $("#search").autocomplete({
                 source: mainbrand
                 });
                 });
    </script>
        <script type="text/javascript">
                $(document).ready(function(){
                 function search(){
                      var title=$("#search").val();
                      if(title!=""){
                         $.ajax({
                            type:"post",
                            url:"ajax_company.php",
                            data:"action=company_search&title="+title,
                            success:function(data){
                                $(".loadmore").html(data);
                               
                             }
                          });
                      } else{      
                            $(".errorMsg").remove();
                            $('#search').after('<label style="color:#990000;" class="errorMsg">Company Cannot be empty.</label>');
                            $(".errorMsg").slideDown(500);
                            setTimeout('$(".errorMsg").slideUp("slow")', 5000);
                            return false;
                      }     
                 }
 
                  $("#button").click(function(){
                     search();
                  });
            });
        </script>        
      
    </div>
</body>

</html>