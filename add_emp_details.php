<?php
include('config.php');
session_start();
$name = $_SESSION["empuser_name"];
$id = $_SESSION['empuser_id'];
?><!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
<![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ScriptsBundle">
    <title>Opportunities A Mega Job Board Template</title>   
    
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">

    <!-- BOOTSTRAPE STYLESHEET CSS FILES -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- JQUERY SELECT -->
    <link href="css/select2.min.css" rel="stylesheet" />
    <!-- JQUERY MENU -->
    <link rel="stylesheet" href="css/mega_menu.min.css">

    <!-- ANIMATION -->
    <link rel="stylesheet" href="css/animate.min.css">

    <!-- OWl  CAROUSEL-->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.style.css">

    <!-- TEMPLATE CORE CSS -->
    <link rel="stylesheet" href="css/style.css">

    <!-- FONT AWESOME -->
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/et-line-fonts.css" type="text/css">

    <!-- Google Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700,900,300" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet" type="text/css">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.css">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css"> 
    
    <script src="js/modernizr.js"></script>
    
</head>

<body>
    
    <div class="page category-page">
        <div id="spinner">
            <div class="spinner-img">
                <img alt="Opportunities Preloader" src="images/loader.gif" />
                <h2>Please Wait.....</h2>
            </div>
        </div>
       <?php 
       @include("top.php");
       ?>
        <div class="clearfix"></div>

        <div class="search">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 nopadding">
                        <div class="input-group">
                            <div class="input-group-btn search-panel">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <span id="search_concept">Filter By</span> <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">By Company</a></li>
                                    <li><a href="#">By Function</a></li>
                                    <li><a href="#">By City </a></li>
                                    <li><a href="#">By Salary </a></li>
                                    <li><a href="#">By Industry</a></li>
                                </ul>
                            </div>
                            <input type="hidden" name="search_param" value="all" id="search_param">
                            <input type="text" class="form-control search-field" name="x" placeholder="Search term...">
                            <span class="input-group-btn">
                        <button class="btn btn-default" type="button"><span class="fa fa-search"></span></button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <section class="job-breadcrumb">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-7 co-xs-12 text-left">
                        <h3>Edit Your Profile</h3>
                    </div>
                    <div class="col-md-6 col-sm-5 co-xs-12 text-right">
                        <div class="bread">
                            <ol class="breadcrumb">
                                <li><a href="#">Home</a>
                                </li>
                                <li><a href="#">Dashboard</a>
                                </li>
                                <li class="active">Edit Profile</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="dashboard-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 nopadding">
                        
                        
                            <div class="heading-inner first-heading">
                                <p class="title">ADD Profile</p>
                            </div>

                            <div class="profile-edit row">
                                <form id="form123" name="form123" method="POST" enctype="multipart/form-data">
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                           
                                            <label>Employername: <span class="required">*</span></label>
                                            <input type="text" id="indusType" placeholder=""  class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label>Type of Business Entity <span class="required">*</span></label>
                                            <input type="text" id="company_type" placeholder=""  class="form-control">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label>No. of Employees <span class="required">*</span></label>
                                            <input type="text" id="worker" placeholder=""  class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-12">
                                        <div class="input-group image-preview form-group">
                                            <label>Profile Image: <span class="required">*</span></label>
                                            <input type="text"  id="logo" name="fupload" placeholder="Upload Profile Image" class="form-control image-preview-filename" disabled="disabled">
                                            <span class="input-group-btn">
                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                        <span class="glyphicon glyphicon-remove"></span> Clear
                                            </button>
                                            <div class="btn btn-default image-preview-input">
                                                <span class="glyphicon glyphicon-folder-open"></span>
                                                <span class="image-preview-input-title">Browse</span>
                                                <input type="file" accept="file_extension" name="img" id="img" />
                                            </div>
                                            </span>
                                        </div>
                                    </div>
                                   
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label>Phone: <span class="required">*</span></label>
                                            <input type="text" id="phone" placeholder=""  id="phone" class="form-control">
                                        </div>
                                    </div>
                                      <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label>Industry Type: <span class="required">*</span></label>
                                      
   
      
    <select id="type" class="questions-category form-control" multiple="multiple">
                                            <?php 
                                        $sql="select * from `tbl_industry_type`";
                                        $res=mysql_query($sql);
                                            while($row=mysql_fetch_assoc($res))
                                            {
                                        
                                                //$roles=explode(",", $row123['fld_industry_type']);
                                       // print_r($roles);
                                        
                                                
                                             ?>
                                                <option    value="<?php echo $row['fld_industrytype'];?>" ><?php echo $row['fld_industrytype'];?></option><?php
                                            
                                  }?></select>
                                        </div>
                                      </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label>Located: <span class="required">*</span></label>
                                      
   
      
<select class="questions-category form-control"  multiple="true"  id="city" >
    <?php
   
        
   $sql="select fld_name from tbl_cities";
   $res=mysql_query($sql);  
   while($rows=mysql_fetch_assoc($res))           
   {   
      // $roles=explode(",", $row['fld_city']);
   ?>
   <option value="<?php echo $rows['fld_name'];?>" ><?php echo $rows['fld_name'];?></option><?php
    

   }  ?>
    </select>
                                        </div>
                                    </div>
                                     
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label>Country <span class="required">*</span></label>
                                            <select class="questions-category form-control"  multiple="true"  id="country" >
    <?php
   
        
   $sql="select fld_name from tbl_countries";
   $res=mysql_query($sql);  
   while($rows=mysql_fetch_assoc($res))           
   {   
       $roles=explode(",", $row['fld_country']);
         foreach($roles as $role)
                                                {
   ?>
                                                <option <?php if($role ==$rows['fld_name']){echo("selected");}}?>value="<?php echo $rows['fld_name'];?>" ><?php echo $rows['fld_name'];?></option><?php
    

   }  ?>
    </select>
                     
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <label>Address <span class="required">*</span></label>
                                            <input type="text" id="address" placeholder=""  class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <label>About Company <span class="required">*</span></label>
                                            <textarea cols="6"  id="company_desc"  rows="8" placeholder=""  class="form-control" > </textarea>
                                        </div>
                                            </div>
                                    <div class="col-md-12 col-sm-12">
<!--                                         <button id="update" class="btn btn-default pull-right" onClick="fn_update(1)"> Save Profile <i class="fa fa-angle-right"></i></button>-->
                                         <input type="button" id="update" class="btn btn-default pull-right" onClick="fn_update(<?php echo $id;?>)" value="Save Profile"/>
                                    </div>
                                </form>
                            </div>
                        
                    </div>
                </div>
            </div>
        </section>

      <?php 
       @include("bottom.php");
       ?>

    <a href="#" class="scrollup"><i class="fa fa-chevron-up"></i></a>
    <script type="text/javascript "src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    
    <script src="http://malsup.github.com/jquery.form.js"></script>        
    
    <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.js"></script>
    <script>
    function fn_update(updateid)
    {
                var indusType = $("#indusType").val();                
        var company_type = $("#company_type").val();        
        var worker = $("#worker").val();
                var country = $("#country").val();
                var company_desc = $("#company_desc").val();
                var city = $("#city").val();
                  var phone = $("#phone").val();
                   var address = $("#address").val();
                    var type = $("#type").val();
                  alert(country);
                if(indusType == ""){            
               
                $("#indusType").focus();
                return false;
                }       
                else if(company_type == "0"){           
               
                $("#company_type").focus();
                return false;           
                }       
                else if(worker == ""){
               
                $("#worker").focus();
                return false;           
                }   
                 else if(phone == ""){
               
                $("#phone").focus();
                return false;           
                }       
                 else if(company_desc == ""){
                
                $("#company_desc").focus();
                return false;           
                }       
                 else if(city == ""){
                
                $("#city").focus();
                return false;           
                }       
                 else if(img == ""){
                
                $("#img").focus();
                return false;           
                }       
                else
                {
                   

        
             $("#form123").ajaxForm({
               type: "POST",
               url: "featured-jobs.php?op=adddetails",
               //data: "indusType="+indusType+"&company_type="+company_type+"&worker="+worker+"&phone="+phone+"&company_desc="+company_desc,
                            data : {indusType: indusType, company_type: company_type , worker: worker, country: country, company_desc: company_desc, city: city,id: updateid,address:address,type:type,phone: phone},
               success: function(data){  
                               alert(data);
                             
                            
                              
                               $(location).attr('href', 'company-dashboard.php');
                               
               },
               beforeSend:function()
               {
                              
                $("#infoerror").css('display', 'inline', 'important');
                $("#infoerror").html("<center><img src='ajax.gif' /> Loading...</center>");
                
               }
              }).submit();  
                          }
        }

    </script>   
         <script>
    jQuery(document).ready(function(){
    jQuery(".chosen").chosen();
    });    
    </script>  
    
        <!-- BOOTSTRAP CORE JS -->
        <script type="text/javascript" src="js/bootstrap.min.js"></script>

        <!-- JQUERY SELECT -->
        <script type="text/javascript" src="js/select2.min.js"></script>
        
        <!-- MEGA MENU -->
        <script type="text/javascript" src="js/mega_menu.min.js"></script>

        <!-- JQUERY COUNTERUP -->
        <script type="text/javascript" src="js/counterup.js"></script>

        <!-- JQUERY WAYPOINT -->
        <script type="text/javascript" src="js/waypoints.min.js"></script>

        <!-- JQUERY REVEAL -->
        <script type="text/javascript" src="js/footer-reveal.min.js"></script>

        <!-- Owl Carousel -->
        <script type="text/javascript" src="js/owl-carousel.js"></script>

        <!-- CORE JS -->
        <script type="text/javascript" src="js/custom.js"></script>

    </div>
</body>
</html>