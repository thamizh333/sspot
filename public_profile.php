<?php
include('config.php');
session_start();
$name = $_SESSION["empuser_name"];
$id = $_SESSION['empuser_id'];

$rurl = $_SERVER['REQUEST_URI'];
    
$ppname1 = explode("?", $rurl);
$ppname = mysql_real_escape_string($ppname1[1]);

$ppnameqry = "select * from tbl_jobseeker where fld_public_link = '$ppname'";
$getqry = mysql_query($ppnameqry);
$rows=mysql_fetch_assoc($getqry);

$plinkid = $rows['fld_id'];
$plinkname = $rows['fld_public_link'];
$plinkusername1 = $rows['fld_name'];
$plinkusername11 = strtolower($plinkusername1);
$plinkusername = ucfirst($plinkusername11);
$plinkjsstatus = $rows['fld_js_status'];


$ppnamefetchingqry = "select * from tbl_jobseeker where fld_public_link = '$ppname'";
$ppnamefetchingqry1 = mysql_query($ppnamefetchingqry);
$rowsppname = mysql_fetch_assoc($ppnamefetchingqry1);



?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ScriptsBundle">
    <title><?php echo $plinkusername; ?>'s  Public Profile | Staffingspot | Job Portal</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- BOOTSTRAPE STYLESHEET CSS FILES -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- JQUERY SELECT -->
    <link href="css/select2.min.css" rel="stylesheet" />
    <!-- JQUERY MENU -->
    <link rel="stylesheet" href="css/mega_menu.min.css">

    <!-- ANIMATION -->
    <link rel="stylesheet" href="css/animate.min.css">

    <!-- OWl  CAROUSEL-->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.style.css">

    <!-- TEMPLATE CORE CSS -->
    <link rel="stylesheet" href="css/style.css">

    <!-- FONT AWESOME -->
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/et-line-fonts.css" type="text/css">

    <!-- Google Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700,900,300" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet" type="text/css">

    <!-- JavaScripts -->
    <script src="js/modernizr.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <div class="page category-page">
        <div id="spinner">
            <div class="spinner-img">
                <img alt="Opportunities Preloader" src="images/loader.gif" />
                <h2>Please Wait.....</h2>
            </div>
        </div>
        <?php
        @include("top.php");
        ?>
    <div class="clearfix"></div>

        <div class="clearfix"></div>
        
        <section class="job-breadcrumb">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-7 co-xs-12 text-left">
                        <h3><?php echo $plinkusername; ?></h3>
                    </div>
                    <div class="col-md-6 col-sm-5 co-xs-12 text-right">
                        <div class="bread">
                            <ol class="breadcrumb">
                                <li><a href="index.php">Home</a> </li>
                                <li class="active">Public Profile</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="light-grey">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 nopadding">
                        <div class="col-md-4 col-sm-4 col-xs-12 col-md-push-8">
                            <?PHP
                             $sql="select * from tbl_jobseeker j join tbl_jobseeker_details jd on(j.fld_id=jd.fld_js_id)   join tbl_social_link sl on(j.fld_id=sl.fld_seekerid)   where j.fld_public_link='".$ppname."'";
 //echo $sql;
  $res=mysql_query($sql);
  while($rows=mysql_fetch_assoc($res))
  {
 ?>
                                <div class="row">
                                    <div class="col-md-10 col-xs-12 col-sm-4">
                                        <div class="about-image" style="padding-left:30px;">
                                <img class="img-responsive" src="images/profilepic/<?php echo $rows[fld_profilepic];?>" alt="" style="margin-left: 10%;">
                                        </div>
                                    </div>
                                    <div class="col-md-10 col-xs-12 col-sm-4">
                                             <div class="resume-social">
                                <ul class="social-network social-circle onwhite">
                                    <li><a href="<?php echo $rows['fld_url']; ?>" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="<?php echo $rows['fld_url']; ?>" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="<?php echo $rows['fld_url']; ?>" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="<?php echo $rows['fld_url']; ?>" class="icoLinkedin" title="Linkedin +"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                                        
                                    </div>
                                   
                                    <div class="col-md-12 col-xs-12 col-sm-4">
                                        <div class="my-contact">
                                       
                                            
                                        </div>
                                    </div>
                                </div>
                            
                            
                        </div>
  <?php }?>
                        <?php 
  $sql="select * from tbl_jobseeker j join tbl_jobseeker_details jd on(j.fld_id=jd.fld_js_id)  where j.fld_public_link='".$ppname."'";
 //echo $sql;
  $res=mysql_query($sql);
  while($rows=mysql_fetch_assoc($res))
  {
 ?>
                        <div class="col-md-8 col-sm-8 col-xs-12 col-md-pull-4">

                            <div class="resume-box">
                                <div class="heading-inner">
                                    <p class="title light-grey">Personal Information</p>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-xs-12 col-sm-4">
                                        <div class="my-contact">
                                            <div class="contact-icon">
                                                <span class="icon-profile-female"></span>
                                            </div>
                                            <div class="contact-info">
                                                <h4>Name: </h4>
                                                <p><?php echo $rows['fld_name']; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12 col-sm-4">
                                        <div class="my-contact">
                                            <div class="contact-icon">
                                                <span class=" icon-envelope"></span>
                                            </div>
                                            <div class="contact-info">
                                                <h4>Email: </h4>
                                                <p><a href="mailto:<?php echo $rows['fld_email']; ?>"><?php echo $rows['fld_email']; ?></a></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12 col-sm-4">
                                        <div class="my-contact">
                                            <div class="contact-icon">
                                                <span class=" icon-phone"></span>
                                            </div>
                                            <div class="contact-info">
                                                <h4>Phone: </h4>
                                                <p><a href="tel:+9911154849901"><?php echo $rows['fld_mobile']; ?></a></p>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12 col-sm-4">
                                        <div class="my-contact">
                                            <div class="contact-icon">
                                                <span class=" icon-envelope"></span>
                                            </div>
                                            <div class="contact-info">
                                                <h4>Address: </h4>
                                                <p><?php echo $rows['fld_address']; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           
  <?php }
 ?>
                                
                                <p class="about-me"><?php echo $rows['fld_aboutmyself']; ?></p>
                        </div>
  <?php// } ?>
                              <?php
  $sql="select * from tbl_jobseeker j join tbl_educationals_details ed on(j.fld_id=ed.fld_seekerid) join tbl_educations e on(e.fld_id=ed.fld_educational)   where j.fld_public_link='".$ppname."' order by ed.fld_from desc";
  //echo $sql;
  $res=mysql_query($sql);
 ?>
  <div class="col-md-12 col-sm-12 col-xs-12">

                            <div class="resume-box">
                                <div class="heading-inner">
                                    <p class="title light-grey">Educational Information</p>
                                    
                                </div>
                              
                        <?php  while($rows=mysql_fetch_assoc($res))
  { ?>
                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                            <span class="icon-clipboard"></span>
                                        </div>
                                        <div class="insti-name">
                                            <h4><?php echo $rows['fld_education'].' '. $rows['fld_course']; ?></h4>
                                             <h4><?php echo $rows['fld_college']; ?></h4>
                                            <span><?php echo $rows['fld_from'].' -'. $rows['fld_to']; ?></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4>Master of Business Administration</h4>
                                            <p>Lesed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in repreh. Excepteur sint occaecat cupidatat non proident. </p>
                                        </div>
                                    </div>
                                </div>
  <?php }?>
                            </div>
                        </div>

                            <?php
  $sql="select * from tbl_jobseeker j join tbl_seeker_experience se on(j.fld_id=se.fld_seekerid)  where j.fld_public_link='".$ppname."' order by se.fld_fromdate desc";
  //echo $sql;
  $res=mysql_query($sql);
  ?>                  <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="resume-box">
                                <div class="heading-inner">
                                    <p class="title light-grey">Work Experience</p>
                                </div>
                                <?php while($rows=mysql_fetch_assoc($res))
  { ?>
      
                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                            <span class="icon-clipboard"></span>
                                        </div>
                                        <div class="insti-name">
                                            <h4><?php echo $rows['fld_companyname']; ?></h4>
                                         <span><?php echo $rows['fld_fromdate'].'   to   '.$rows['fld_todate']; ?></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4><?php echo $rows['fld_designation']; ?></h4>
                                            <p>Lesed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in repreh. Excepteur sint occaecat cupidatat non proident. </p>
                                        </div>
                                    </div>
                                </div>
                                
                              <?php }?>   
                            </div>
                        </div>
 
                             <?php
  $sql="select * from tbl_jobseeker j join tbl_job_skills ed on(j.fld_id=ed.fld_seekerid)  where j.fld_public_link='".$ppname."' ";
  //echo $sql;
  $res=mysql_query($sql);
  ?>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="heading-inner">
                                <p class="title light-grey">Skills That I have</p>
                            </div>
                     
                            <div class="row">
                                 <?php      while($rows=mysql_fetch_assoc($res))
  {?>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                      <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $rows['fld_percentage']; ?>%;">
                                            <span class="sr-only"><?php echo $rows['fld_percentage']; ?></span>
                                        </div>
                                        <span class="progress-type"><?php echo $rows['fld_skill']; ?></span>
                                        <span class="progress-completed"><?php echo $rows['fld_percentage']; ?>%</span>
                                    </div>
<!--                                      <div class="progress">
                                        <div class="progress-bar progress-bar-info"role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $rows['fld_percentage']; ?>%;">
                                            <span class="sr-only"><?php echo $rows['fld_percentage']; ?></span>
                                        </div>
                                        <span class="progress-type"><?php echo $rows['fld_skill']; ?></span>
                                        <span class="progress-completed"><?php echo $rows['fld_percentage']; ?>%</span>
                                    </div>-->
                          
                                </div>
                                
                               <?php }?>
                                
                            </div>
        
                        </div>
                        

                   
                </div>
            </div>
        </section>
  <?php //} ?>

    <?php @include("bottom.php");?>

        <!-- JAVASCRIPT JS  -->
        <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
    
    <!-- JAVASCRIPT JS  --> 
    <script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>

        <!-- BOOTSTRAP CORE JS -->
        <script type="text/javascript" src="js/bootstrap.min.js"></script>

        <!-- JQUERY SELECT -->
        <script type="text/javascript" src="js/select2.min.js"></script>
        <!-- MEGA MENU -->
        <script type="text/javascript" src="js/mega_menu.min.js"></script>

         

        <!-- JQUERY COUNTERUP -->
        <script type="text/javascript" src="js/counterup.js"></script>

        <!-- JQUERY WAYPOINT -->
        <script type="text/javascript" src="js/waypoints.min.js"></script>

        <!-- JQUERY REVEAL -->
        <script type="text/javascript" src="js/footer-reveal.min.js"></script>

        <!-- Owl Carousel -->
        <script type="text/javascript" src="js/owl-carousel.js"></script>

        <!-- CORE JS -->
        <script type="text/javascript" src="js/custom.js"></script>

    </div>
</body>


<!-- Mirrored from templates.scriptsbundle.com/opportunities-v3/demo/resume2.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 07 Mar 2017 01:56:50 GMT -->
</html>