<?php
include('config.php');
session_start();

$name = $_SESSION["empuser_name"];
$id = $_SESSION['empuser_id'];

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />   
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="vinforma">
    <title>Post Job | Employer | Staffingspot | Job Portal</title>
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">

    <!-- BOOTSTRAPE STYLESHEET CSS FILES -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- JQUERY SELECT -->
    <link href="css/select2.min.css" rel="stylesheet" />

    <!-- JQUERY MENU -->
    <link rel="stylesheet" href="css/mega_menu.min.css">

    <!-- ANIMATION -->
    <link rel="stylesheet" href="css/animate.min.css">

    <!-- OWl  CAROUSEL-->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.style.css">

    <!-- TOASTER CSS -->
    <link rel="stylesheet" href="css/toastr.min.css">
    <link rel="stylesheet" href="css/jquery.tagsinput.min.css">
    <!-- TEMPLATE CORE CSS -->
    <link rel="stylesheet" href="css/style.css">

    <!-- FONT AWESOME -->
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/et-line-fonts.css" type="text/css">

    <!-- Google Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700,900,300" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet" type="text/css">
    
    <!-- JavaScripts -->
    <script src="js/modernizr.js"></script>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<script src="//cdn.ckeditor.com/4.5.5/standard/ckeditor.js"></script>
    <style>
        .Exp{margin-left: -30px;}
        .expyr{width: 130px;}
        .Expyr1{width: 123px;margin-left: -33px;}
        .nav > li > a{font-size: 20px;}
        .fcolr{color: black;}
    </style>

</head>

<body>
    <div class="page category-page">
        <div id="spinner">
            <div class="spinner-img">
                <img alt="Opportunities Preloader" src="images/loader.gif" />
                <h2>Please Wait.....</h2>
            </div>
        </div>

        <?php @include("top.php");?>
        <div class="clearfix"></div>
        <div class="clearfix"></div>
     
        <?php @include("emptop.php");?>
        
        <section class="dashboard-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <?php 
                      @include("empleftpanel.php");
                      ?>
                        <div class="col-md-8 col-sm-12 col-xs-12" id="postjobtab">
                        <div class="Heading-title-left black small-heading">
                            <h3>Post A New job</h3>
                           <!--  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium</p> -->
                        </div>
                        <div class="post-job2-panel">
                            <form class="row" id="form123" enctype="multipart/formdata" method="post" action="">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Job Title</label>
                                        <input type="text" placeholder="Job Title" id="title" class="form-control" style="height:47px">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                <label>Location</label>
                                              
                                <select id="city" class="questions-category form-control" data-placeholder="Select Your City" multiple="true">
                                <?php
                                $sql="select fld_name from tbl_cities";
                                $res=mysql_query($sql);  
                                while($rows=mysql_fetch_assoc($res))           
                                {   
                                ?>
                                <option  value="<?php echo $rows['fld_name'];?>" ><?php echo $rows['fld_name'];?></option><?php


                                }  ?>
                                </select>
               
                                </div>
                                </div>
                              
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Industry Type</label>
                                        
                                       
                                        <select id="type" class="questions-category form-control" data-placeholder="Select Industry Type" multiple="multiple">
                                            <?php 
                                        $sql="select * from `tbl_industry_type`";
                                        $res=mysql_query($sql);
                                            while($row=mysql_fetch_assoc($res))
                                        {
                                        ?>
                                             <option value="<?php echo $row['fld_industrytype'] ?>"><?php echo $row['fld_industrytype'] ?></option>
                                            
                                        
                                        <?php }?></select>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Functional Area</label>
                                        <select id="area" name="area" class="questions-category form-control" data-placeholder="Select Functional Area" multiple="multiple">
                                             <?php 
                                        $sql="select * from `tbl_funtional_area`";
                                        $res=mysql_query($sql);
                                            while($row=mysql_fetch_assoc($res))
                                        {
                                        ?>
                                             <option value="<?php echo $row['fld_fuctionalarea'] ?>"><?php echo $row['fld_fuctionalarea'] ?></option>
                                            
                                        
                                        <?php }?>
                                            
                                        </select>
                                    </div>
                                </div>
                                 <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Role </label>
                                        <select id="role" class="questions-category form-control" data-placeholder="Select Role">
                                            <?php 
                                             $sql="select * from `tbl_role`";
                                        $res=mysql_query($sql);
                                            while($row=mysql_fetch_assoc($res))
                                        {
                                        ?>
                                             <option value="<?php echo $row['fld_role'] ?>"><?php echo $row['fld_role'] ?></option>
                                            
                                        
                                        <?php }?>
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Job Type</label>
                                        <select id="jobtype" class="questions-category form-control" data-placeholder="Select Job Type">
                                            <option value="Full Time">Full Time</option>
                                            <option value="Part Time">Part Time</option>
                                            <option value="Remote">Remote</option>
                                            <option value="Freelancer">Freelancer</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    
                                    <div class="form-group">
                                        <label>Job Experience</label>
                                        <div class="col-md-12 col-sm-12 col-xs-12 Exp">
                                            
                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                                <select  id="exp" class="questions-category form-control" data-placeholder="Select Year">
                                            <option value="0">0</option>
                                            <option value="1">1 </option>
                                            <option value="2">2 </option>
                                            <option value="3">3 </option>
                                            <option value="1">4 </option>
                                            <option value="2">5 </option>
                                            <option value="3">6+ </option>
                                        </select>
                                           </div>
                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                        <label style="margin-top:10px;margin-left: -20px;">year</label>
                                           </div> 
                                            <div class="col-md-3 col-sm-3 col-xs-12"> <select id="exp1" class="questions-category form-control" data-placeholder="Select Month">
                                            <option value="0">0</option>
                                            <option value="1">1 </option>
                                            <option value="2">2 </option>
                                            <option value="3">3 </option>
                                            <option value="4">4 </option>
                                            <option value="5">5 </option>
                                            <option value="6">6</option>
                                            <option value="2">7 </option>
                                            <option value="3">8</option>
                                            <option value="4">9 </option>
                                            <option value="5">10 </option>
                                            <option value="6">11</option>
                                            
                                            
                                        
                                        </select>
                                             </div>
                                            
                                             <div class="col-md-3 col-sm-3 col-xs-12">
                                        <label style="margin-top:10px;margin-left: -20px;" >month</label>
                                           </div> 
                                        </div>
                                       
                                       
                                        
                                    </div>
                                </div>
                                
                                
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    
                                    <div class="form-group">
                                        <label>Expected Salary</label>
                                        <div class="col-md-12 col-sm-12 col-xs-12 Exp">
                                            
                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                                <select id="expyr" class="questions-category form-control" >
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                             <option value="8">8</option>
                                              <option value="9">9</option>
                                              
                                        </select>
                                               
                                           </div>
                                            <div class="col-md-2 col-sm-2 col-xs-12">
                                        <label style="margin-top:10px;margin-left: -20px;">Lacs</label>
                                           </div> 
                                            <div class="col-md-5 col-sm-5 col-xs-12 Expyr1">  <select id="expyr1" class="questions-category form-control">
                                           
                                            <option value="10,000">10,000 +</option>
                                            <option value="20,000">20,000 +</option>
                                            <option value="30,000">30,000 +</option>
                                            <option value="40,000">40,000 +</option>
                                            <option value="50,000 ">50,000 +</option>
                                            <option value="60,000">60,000 +</option>
                                            
                                        </select>
                                             </div>
                                            
                                             <div class="col-md-2 col-sm-2 col-xs-12">
                                        <label style="margin-top:10px;margin-left: -20px;" >Thousands</label>
                                           </div> 
                                        </div>                                        
                                    </div>
                                </div>
                               
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Keywords</label>
                                        <input type="text" id="tags" class="form-control" data-placeholder="Enter Your Skills..." data-role="tagsinput">
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Job Detials</label>
                                        <textarea name="ckeditor" class="ckeditor" id="ckeditor"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <input type='button' class="btn btn-default pull-right" id="postjob" onClick="fn_post();"value="Post Job" >             
                                </div>
                            </form>
                        </div>
                    </div>      

                    <!-- Edit job Tab Start-->

                   





                    </div>
                </div>
            </div>
        </section>

       <?php @include("bottom.php");?>

        <!-- JAVASCRIPT JS  -->
        <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
    
    <!-- JAVASCRIPT JS  --> 
    <script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>

        <!-- BOOTSTRAP CORE JS -->
        <!--<script type="text/javascript" src="js/bootstrap.min.js"></script>-->

       

        <!-- JQUERY SELECT -->
        <script type="text/javascript" src="js/select2.min.js"></script>
        
        <!-- MEGA MENU -->
        <script type="text/javascript" src="js/mega_menu.min.js"></script>

         

        <!-- JQUERY COUNTERUP -->
        <script type="text/javascript" src="js/counterup.js"></script>

        <!-- JQUERY WAYPOINT -->
        <script type="text/javascript" src="js/waypoints.min.js"></script>

        <!-- JQUERY REVEAL -->
        <script type="text/javascript" src="js/footer-reveal.min.js"></script>

        <!-- Owl Carousel -->
        <script type="text/javascript" src="js/owl-carousel.js"></script>

        <!-- TOASTER JS -->
        <script type="text/javascript" src="js/toastr.min.js"></script>

        <!-- CORE JS -->
        <script type="text/javascript" src="js/custom.js"></script>

         <script type="text/javascript" src="js/jquery.tagsinput.min.js"></script>
        <script type="text/javascript">
            $('#tags').tagsInput({
                width: 'auto'
            });
        </script>
       
       <?php
       if($sum==0)
       {
            $resumedetails =  "You Job skills haven't find New Resumes";
       }
       elseif ($sum==1) {
           
           $resumedetails =  "You Job skills got $sum New Resume";
       }
       else
       {
        $resumedetails =  "You Job skills got $sum New Resumes";
       }
       ?>

        <script type="text/javascript">
            toastr.options = {
                "closeButton": true,
                "positionClass": "toast-bottom-left",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr["success"]("<?php echo  $resumedetails; ?>", "Hello <?php echo $empname; ?>", {
                timeOut: 9000
            })
        </script>

<script>
        function fn_post()
        {   
            var title = $("#title").val();
            var city = $("#city").val();
            var type = $("#type").val();
            var area = $("#area").val();
            //alert(area);
            var role = $("#role").val();
            var exp = $("#exp").val();
            var exp1 = $("#exp1").val();
            var expyr = $("#expyr").val();
            var expyr1 = $("#expyr1").val();
            var ckeditor = CKEDITOR.instances['ckeditor'].getData()
            var jobtype = $("#jobtype").val();
            var tags=$("#tags").val();
                  
                if(title == ""){  
                $("#title").focus();
                return false;
                }       
                else if(city == ""){            
                $('#infoerror').css('background-color', 'gray');
                $('#infoerror').css('color', '#fff');
                $("#infoerror").css("display", "block");
                $("#infoerror").html("Please Select City!");
                $("#city").focus();
                return false;           
                }       
                else if(type == ""){
                $('#infoerror').css('background-color', 'gray');
                $('#infoerror').css('color', '#fff');
                $("#infoerror").css("display", "block");
                $("#infoerror").html("Please Select the Date!");
                $("#type").focus();
                return false;           
                }   
                else if(area == ""){
                $('#infoerror').css('background-color', 'gray');
                $('#infoerror').css('color', '#fff');
                $("#infoerror").css("display", "block");
                $("#infoerror").html("Please Select the Date!");
                $("#type").focus();
                return false;           
                }   
                else if(role == ""){
                $('#infoerror').css('background-color', 'gray');
                $('#infoerror').css('color', '#fff');
                $("#infoerror").css("display", "block");
                $("#infoerror").html("Please Select the Date!");
                $("#type").focus();
                return false;           
                }   else if(exp == ""){
                $('#infoerror').css('background-color', 'gray');
                $('#infoerror').css('color', '#fff');
                $("#infoerror").css("display", "block");
                $("#infoerror").html("Please Select the Date!");
                $("#type").focus();
                return false;           
                }   
                else if(exp1 == ""){
                $('#infoerror').css('background-color', 'gray');
                $('#infoerror').css('color', '#fff');
                $("#infoerror").css("display", "block");
                $("#infoerror").html("Please Select the Date!");
                $("#type").focus();
                return false;           
                }   
                else if(expyr == ""){
                $('#infoerror').css('background-color', 'gray');
                $('#infoerror').css('color', '#fff');
                $("#infoerror").css("display", "block");
                $("#infoerror").html("Please Select the Date!");
                $("#type").focus();
                return false;           
                }   
                else if(expyr1 == ""){
                $('#infoerror').css('background-color', 'gray');
                $('#infoerror').css('color', '#fff');
                $("#infoerror").css("display", "block");
                $("#infoerror").html("Please Select the Date!");
                $("#type").focus();
                return false;           
                }   
                
//                
                else
                {
                        $("#infoerror").css("display", "none"); 
                        $("#divmsgbox").html("<div class='msgboxinfo'><img src='ajax-loaderdrop.gif'/></div>");                 
                        //alert(updateid);
                                $.ajax({
                           type: "POST",
                           url: "featured-jobs.php?op=addinfo",
                           //data: "infoupdateid="+updateid+"&info="+investinfodetails+"&linkurl="+linkurl+"&infodate="+infodate,
                          // data : {infoupdateid: updateid , info: investinfodetails, linkurl: linkurl, infodate: infodate},
                           data : {title: title, city: city , type: type, area: area, role: role, exp1: exp1,exp: exp, expyr: expyr, expyr1: expyr1, tags: tags, jobtype: jobtype, ckeditor: ckeditor},
                           success: function(data){ 
                               //alert(data);
                               //window.Location('company-dashboard-featured-jobs.php');
                               $(location).attr('href', 'company-dashboard-featured-jobs.php');
                                //alert(data);
                               // location.reload();
//                                $("#infoerror").css("display", "block");
//                                $("#infoerror").html("Investor Details are Updated Succesfully");
//                                //$("#form1").trigger('reset');               
//                                $("#results").load("fetch_pages_investor_info.php");
//                                $("#infodetailslisting").load(window.location + " #infodetailslisting");
//                                $("#newinfodetailsinside").load(window.location + " #newinfodetailsinside");
//                               
                                                               
                                
                           },
                           
                         });
                }
}

       </script>
        


    </div>
</body>
</html>